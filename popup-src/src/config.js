const configs = {
  INTERVAL_TIME_SECONDS: 3,
  STEEXP_EXPLORER: 'https://steexp.com',
  HORIZON_API: 'https://horizon.stellar.org',
  HORIZON_TESTNET_API: 'https://horizon-testnet.stellar.org',
};

export default configs;
